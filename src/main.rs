extern crate kankyo;
extern crate reqwest;
extern crate serenity;
extern crate local_ip;
#[macro_use(struct_with_default)]
extern crate struct_with_default;

mod error;

use std::{
    env,
    string::String,
    fs::File,
    error::Error,
};
use serenity::{
    model::{
        channel::Message,
        gateway::Ready,
        misc::Mentionable,
    },
    prelude::Context as DiscordContext,
    prelude::EventHandler as DiscordEventHandler,
    prelude::TypeMapKey,
};
use error::SimpleError;
use std::process::exit;

struct Bot {
    config: Config,
}

impl DiscordEventHandler for Bot {
    fn message(&self, ctx: DiscordContext, msg: Message) {
        let content = msg.content.as_str();
        let api_url = "https://duckduckgo.com/html/?q=";
        let api_options = "&kp=1&kl=us%2Dus";
        if content.starts_with(&self.config.bang_prefix) {
            let command = content[self.config.bang_prefix.len()..].to_string();
            let mut split = command.splitn(2, ' ');
            let bang = split.next().expect("Expected bang command");
            let maybe_query = split.next().map(|s| s.replace(" ", "+"));

            /*if bang == "ddg" || bang == "ddgi" {
                return; // TODO: handle special case (DDG API)
            }*/

            let request_url = match maybe_query {
                Some(query) => format!("{}!{}+{}{}", api_url, bang, query, api_options),
                None => format!("{}!{}{}", api_url, bang, api_options),
            };

            msg.channel_id.broadcast_typing(&ctx.http).ok();
            let res = match follow_http_redirect(&request_url) {
                Ok(redirect_url) => {
                    println!("!{} => {} => {}", command, request_url, redirect_url);

                    if request_url == redirect_url { // no redirect took place => no results
                        msg.reply(ctx.http, "This is not a valid bang command")
                    } else {
                        msg.reply(ctx.http, &redirect_url)
                    }
                },
                Err(e) => {
                    msg.reply(ctx.http, format!("Failed to find destination: {}", e))
                }
            };

            if let Err(err) = res {
                println!("Error: {}", err);
            }

        } else if let Ok(current_user) = ctx.http.get_current_user() {
            if msg.mentions_user_id(current_user.id) {
                let command = content.replacen(current_user.mention().as_str(), "", 1);
                if command.contains("reboot") {
                    if self.config.owner_id != 0 && msg.author.id.0 == self.config.owner_id {
                        msg.reply(ctx.http, "Okay, shutting down...").ok();
                        exit(0); // reboot happens automatically through systemd
                    } else {
                        msg.reply(ctx.http, "Only my owner may do that").ok();
                    }
                } else if command.contains("ip") {
                    if self.config.owner_id != 0 && msg.author.id.0 == self.config.owner_id {
                        match get_ips() {
                            Ok((local, public)) => {
                                msg.reply(ctx.http, format!("\nLocal: {}\nPublic: {}", local, public)).ok();
                            },
                            Err(e) => {
                                msg.reply(ctx.http, format!("Something went wrong: {:?}", e));
                            }
                        }
                    } else {
                        msg.reply(ctx.http, "Only my owner may do that").ok();
                    }
                } else if command.contains("help") {
                    msg.reply(ctx.http, format!("DuckDuckGo search bot, created by Sinthorion.\n\
                        Usage: \n\
                        - `{}<bang> [query]`: Returns the resulting URL of search the specified bang command in DDG.\n\
                                            omit <bang> (put space after prefix) to search for first DDG result for query.",
                        &self.config.bang_prefix)).ok();
                }
            }
        }
    }

    fn ready(&self, _ctx: DiscordContext, _bot: Ready) {
        println!("Duck duck gooooooo!");
    }
}

fn follow_http_redirect(url: &str) -> Result<String, Box<dyn Error>> {
    match reqwest::get(url) {
        Ok(response) => Ok(response.url().to_string()),
        Err(e) => {
            e.url().map(|url| url.to_string()).ok_or(Box::new(e))
        }
    }
}

fn get_ips() -> Result<(String, String), Box<dyn Error>> {
    let local_ip = local_ip::get().ok_or(Box::new(SimpleError::new("Failed to fetch local IP")))?.to_string();
    let public_ip = reqwest::get("https://api.ipify.org").map_err(|e| Box::new(e))?.text()?;
    Ok((local_ip, public_ip))
}

struct_with_default!{
#[derive(Clone)]
struct Config {
    token: String,
    bang_prefix: String = "!".to_string(),
    owner_id: u64,
}
}

impl TypeMapKey for Config {
    type Value = Config;
}

fn load_config() -> Result<Config, Box<dyn Error>> {
    let mut config = Config::default();
    if let Ok(mut file) = File::open("config.env") {
        kankyo::load_from_reader(&mut file, true)?;
    }

    if let Ok(token) = env::var("DISCORD_TOKEN") {
        config.token = token;
    } else {
        return Err(Box::new(SimpleError::new("Expected discord token in the environment")));
    }
    if let Ok(bang_prefix) = env::var("DDG_BANG") {
        config.bang_prefix = bang_prefix;
    }
    if let Ok(owner_id_str) = env::var("OWNER_ID") {
        if let Ok(owner_id) = owner_id_str.parse::<u64>() {
            config.owner_id = owner_id;
        }
    }

    return Ok(config);
}

fn main() -> Result<(), Box<dyn Error>> {
    let handler = Bot {
        config: load_config()?
    };
    let token = handler.config.token.clone();

    let mut client = serenity::Client::new(&token, handler)?;

    if let Err(why) = client.start() {
        eprintln!("Client error: {:?}", why);
    }

    Ok(())
}
