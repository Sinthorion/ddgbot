
use std::{
    error::Error,
    fmt::{Display, Formatter, Result},
};

#[derive(Debug)]
pub struct SimpleError<'a> {
    message: &'a str,
}

impl <'a> SimpleError<'a> {
    pub fn new(message: &'a str) -> SimpleError {
        SimpleError {
            message: message
        }
    }
}

impl <'a> Error for SimpleError<'a> {}

impl <'a> Display for SimpleError<'a> {
    fn fmt(&self, f: &mut Formatter) -> Result {
        f.write_str(self.message)
    }
}